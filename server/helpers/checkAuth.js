const jwt = require("jsonwebtoken");
const config = require("../config/config");

module.exports = (req, res, next) => {
    try {
        // console.log(req.headers["authorization"]);
        const token = req.headers["authorization"].split(" ")[1];
        const decoded = jwt.verify(token, config.secret);
        req.userInfo = decoded;
        next();
    } catch(err) {
        return res.json(false);
    }
};