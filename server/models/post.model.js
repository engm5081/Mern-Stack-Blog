const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const post = new Schema({
    text: {
        type: String,
        required: true
    },
    publisherId: {
        type: String,
        required: true
    },
    publisherUsernamer: {
        type: String,
        required: true
    },
    comments: {
        type: Array,
        required: false,
        default: []
    },
    shares: {
        type: Array,
        required: false,
        default: []
    },
    likes: {
        type: Array,
        required: false,
        default: []
    },
    date: {
        type: Date,
        default: new Date().getTime()
    }
});

mongoose.model("post", post);