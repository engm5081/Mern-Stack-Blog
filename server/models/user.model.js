const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const user = new Schema({
    username: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    roles: {
        type: Array,
        required: false,
        default: []
    },
    followers: {
        type: Array,
        required: false,
        default: []
    },
    following: {
        type: Array,
        required: false,
        default: []
    },
    recoveryRequestNumber: {
        type: Number,
        required: false,
        default: null
    },
    timeNow: {
        type: Date,
        required: false,
        default: null
    },
    date: {
        type: Date,
        default: new Date().getTime()
    }
});

mongoose.model("user", user);