const express = require('express');
const mongoose = require("mongoose");
const router = express.Router();
const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const usernameRegex = /^[a-zA-Z\-]+$/;
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = mongoose.model("user");
const Post = mongoose.model("post");
const config = require("../config/config");
const authCheck  = require("../helpers/checkAuth");
const message = require("../helpers/message");


router.post("/register", (req, res) => {
    const {username, email, password, re_password} = req.body;
    let valid = true;
    valid = valid && 
            emailRegex.test(email) && 
            usernameRegex.test(username) && 
            password === re_password && 
            password.length >= 6;
    
    if (valid) {
        User.findOne({email}, (err, user) => {
            if (err) {
                return res.json({
                    success: true
                });
            }
            // console.log(user);
            if (!user) {
                const hashedPassword = bcrypt.hashSync(password, 8);
                new User({
                    username,
                    email,
                    password: hashedPassword
                })
                    .save()
                    .then(_ => {
                        return res.json({
                            success: true
                        });
                    })
                    .catch(_ => {
                        return res.json({
                            success: false
                        });
                    })
            } else {
                return res.json({
                    success: false
                });
            }
        });
    } else {
        return res.json({
            success: false
        });
    }
});

router.post("/login", (req, res) => {
    const {email, password} = req.body;

    User.findOne({email}, (err, user) => {
        if (err) {
            return res.json({
                success: false
            });
        }
        if (user) {
            bcrypt.compare(password, user.password, function(err, result) {
                if (err) {
                    return res.json({
                        success: false
                    });
                }
                if (result) {
                    const token = jwt.sign({id: user._id}, config.secret, {
                        expiresIn: 7200
                    });
                    return res.json({
                        expiresIn: new Date().getTime() + (7200 * 1000),
                        success: true,
                        email,
                        username: user.username,
                        token,
                        id: user._id,
                        roles: user.roles,
                        following: user.following
                    });
                } else {
                    return res.json({
                        success: false
                    });
                }
            });
        } else {
            return res.json({
                success: false
            });
        }
    });
});

router.post("/forgotpass", (req, res) => {
    message(req, res);
});


router.get("/:follow", (req, res) => {
    const {follow} = req.params;
    if (follow !== "followers" && follow !== "following") return res.json(false);
    const {id} = req.headers;
    User.findOne({_id: id}, (err, user) => {
        if (err || !user) return res.json(false);
        res.json({
            [follow]: user[follow],
            username: user.username
        });
    });
});

router.post("/follow", authCheck, (req, res) => {
    const {followingId, followersId} = req.body;
    User.findOne({_id: followingId}, (err1, user) => {
        if (err1 || !user) return res.json(false);
        User.findOne({_id: followersId}, (err2, userFollower) => {
            if (err2 || !userFollower) return res.json(false);
            const {followers} = user;
            let followerIndex = -1;
            for (let i = 0, max = followers.length; i < max; i++) {
                if (followers[i].id == followersId) {
                    followerIndex = i;
                    break;
                }
            }
            if (followerIndex === -1) {
                followers.push({
                    id: userFollower._id,
                    username: userFollower.username
                });
            } else {
                followers.splice(followers.indexOf(followersId), 1);
            }
            User.findOneAndUpdate({_id: followingId}, {$set: {followers}}, {new: true}, (err3, user1) => {
                if (err3 || !user1) return res.json(false);
                const {following} = userFollower;
                let followingIndex = -1;
                for (let i = 0, max = following.length; i < max; i++) {
                    if (following[i].id == followingId) {
                        followingIndex = i;
                        break;
                    }
                }
                 if (followingIndex === -1) {
                    following.push({
                        id: user._id,
                        username: user.username
                    });
                } else {
                    following.splice(following.indexOf(followersId), 1);
                }
                User.findOneAndUpdate({_id: followersId}, {$set: {following}}, {new: true}, (err4, user2) => {
                    if (err4 || !user2) return res.json(false);
                    res.json(user2);
                });
            });
        });
    });
});

router.post("/myposts", authCheck, (req, res) => {
    const {publisherId} = req.body;
    User.findOne({_id: publisherId}, (err, user) => {
        if (err || !user) return res.json(false);
        const {username} = user;
        Post.find({publisherId}, (err, posts) => {
            if (err || !posts) return res.json(false);
            res.json({username, posts: posts});
        })
    });
});

router.get("/forget/:number/:id", (req, res) => {
    const {number, id} = req.params;
    User.findOne({_id: id}, (err, user) => {
        if (err || !user || user.recoveryRequestNumber != number) return res.json(false);
        if (new Date().getTime() - new Date(user.timeNow).getTime() > 12 * 60 * 60 * 1000) return res.json({msg: "Page No Longer Available Please Require New One"});
        res.json(true);
    });
});

router.post("/forget/:id", (req, res) => {
    const {pass, re_pass, number, id} = req.body;
    if (pass !== re_pass || pass.length < 6) return res.json(false);
    User.findOne({_id: id}, (err, user) => {
        if (err || !user || user.recoveryRequestNumber != number) return res.json(false);
        const password = bcrypt.hashSync(pass, 8);
        User.findOneAndUpdate({_id: id}, {$set: {password, recoveryRequestNumber:null, timeNow: null}}, {new: true}, (err, updatedUser) => {
            if (err || !updatedUser) return res.json(false);
            res.json(true);
        });
    });
});

module.exports = router;