const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const Post = mongoose.model("post");
const User = mongoose.model("user");
const striptags = require('striptags');
const authCheck = require("../helpers/checkAuth");


router.get("/main/posts", authCheck, (_, res) => {
    Post.find({}, (err, posts) => {
        if (err) return res.json(false);
        const sendPosts = posts || [];
        res.json(sendPosts);
    });

});

router.post("/post/add", authCheck, (req, res) => {
    const {id, username} = req.body;
    const text = striptags(req.body.text, ['br']);
    if (!id || !username || !text) return res.json(false); 
    console.log("test");
    new Post({
        text,
        publisherId: id,
        publisherUsernamer: username
    })
        .save()
        .then(post => {
            res.json(post);
        })
        .catch(err => {
            res.json(false);
        })
});

router.post("/post/like", authCheck, (req, res) => {
    const {postId, id} = req.body;
    Post.findOne({_id: postId}, (err, post) => {
        if (err || !post) return res.json(false);
        let likes = [...post.likes];
        let checkFoundOrNot = -1;
        for (let i = 0; i < likes.length; i++) {
            if (likes[i].id === id) {
                checkFoundOrNot = i;
                break;
            }
        }
        if (checkFoundOrNot === -1) {
            likes.push({id});
        } else {
            likes.splice(checkFoundOrNot, 1);
        }
        Post.findOneAndUpdate({_id: postId}, {$set: {likes}}, {new: true}, (err, post) => {
            if (err || !post) return res.json(false);
            res.json(post);
        });
    });
});


router.post("/post/addcomment", authCheck, (req, res) => {
    const {id, username, text, postId} = req.body;
    User.findOne({_id: id}, (err, user) => {
        if (err || !user) return res.json(false);
        Post.findOne({_id: postId}, (err, post) => {
            if (err || !user) return res.json(false);
            const {comments} = post;
            const sentText = striptags(text);
            comments.push({username, text: sentText, id});
            Post.findOneAndUpdate({_id: postId}, {$set: {comments}}, {new: true}, (err, updatedPost) => {
                if (err || !updatedPost) return res.json(false);
                res.json(updatedPost);
            })
        })
    })
});


router.post("/post/share", authCheck, (req, res) => {
    const {id, username, postId} = req.body;
    const text = striptags(req.body.text, ['br']);
    if (!id || !username || !text) return res.json(false); 
    Post.findOneAndUpdate({_id: postId}, {$push: {shares: id}}, {new: true}, (err, updatedPost) => {
        if (err || !updatedPost) return res.json(false);
            
            new Post({
                text,
                publisherId: id,
                publisherUsernamer: username
            })
                .save()
                .then(post => {  
                    res.json({post, updatedPost});
                })
                .catch(_ => {
                    res.json(false);
                })
    })
    
});

module.exports = router;