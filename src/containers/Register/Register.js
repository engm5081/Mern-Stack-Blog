import React, { Component } from 'react';
import Input from '../../components/Form/Input/Input';
import Button from '../../components/Form/Button/Button';
import Loading from '../../components/Loading/Loading';
import "./Register.css";
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as authActions from '../../store/auth/authActions';

class HomeRegister extends Component {

    state = {
        formControls: [
            {
                id: "username-input",
                type: "text",
                placeholder: "Username*",
                iconClass: "register-input-email",
                value: "",
                controlConfirmation: {
                    minLength: 4,
                    touched: false,
                    valid: false,
                    maxLength: 12,
                    // eslint-disable-next-line
                    regex: /^[a-zA-Z\-]+$/,
                    errors: []
                }
            },
            {
                id: "email-input",
                type: "email",
                placeholder: "Your Email*",
                iconClass: "register-input-username",
                value: "",
                controlConfirmation: {
                    touched: false,
                    valid: false,
                    // eslint-disable-next-line
                    regex: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
                    errors: []
                }
            },
            {
                id: "password-input",
                type: "password",
                placeholder: "Password*",
                iconClass: "register-input-password",
                value: "",
                controlConfirmation: {
                    minLength: 6,
                    touched: false,
                    valid: false,
                    errors: []
                }
            },
            {
                id: "re-password-input",
                type: "password",
                placeholder: "Confirm Password*",
                iconClass: "register-input-password",
                value: "",
                controlConfirmation: {
                    minLength: 6,
                    touched: false,
                    valid: false,
                    brotherId: "password-input",
                    errors: []
                }
            }
        ]
    };

    changeHandler = (event, id) => {
        const formControls = [...this.state.formControls];
        const changedIndex = formControls.findIndex(control => control.id === id);
        const changedControl = {...formControls[changedIndex]};
        changedControl.value = event.target.value;
        formControls[changedIndex] = changedControl;
        this.setState({formControls});
    }
   
    focusHandler = (id) => {
        const formControls = [...this.state.formControls];
        const focusedIndex = formControls.findIndex(control => control.id === id);
        const focusedControl = {...formControls[focusedIndex]};
        if (focusedControl.controlConfirmation.touched === false) {
            focusedControl.controlConfirmation.touched = true;
            formControls[focusedIndex] = focusedControl;
            this.setState({formControls});
        }
    }

    blurHandler = (event, id) => {
        const formControls = [...this.state.formControls];
        const bluredIndex = formControls.findIndex(control => control.id === id);
        const bluredControl = {...formControls[bluredIndex]};

        let valid = true;
        let errors = [];
        if (bluredControl.controlConfirmation.minLength) {
            if (bluredControl.controlConfirmation.minLength > event.target.value.length) {
                errors.push(`${bluredControl.controlConfirmation.minLength} is the min length`);
            } 
        }

        if (bluredControl.controlConfirmation.maxLength) {
            if (bluredControl.controlConfirmation.maxLength < event.target.value.length) {
                errors.push(`${bluredControl.controlConfirmation.maxLength} is max Length`);
            }
        }

        if (bluredControl.controlConfirmation.regex) {
            if (bluredControl.type === "email") {
                if (!bluredControl.controlConfirmation.regex.test(event.target.value)) {
                    errors.push(`invalid email`);
                }
            } else {
                if (!bluredControl.controlConfirmation.regex.test(event.target.value)) {
                    errors.push(`password cannot include numbers or special chars`);
                }
            }
        }

        if (bluredControl.controlConfirmation.brotherId) {
            const brotherIndex = formControls.findIndex(control => control.id === bluredControl.controlConfirmation.brotherId);
            const brotherControl = {...formControls[brotherIndex]};
            if (!(brotherControl.value === bluredControl.value)) {
                errors.push("password not matched");
            }
        }

        if (errors.length) {
            valid = false;
        }

        bluredControl.controlConfirmation.errors = errors;
        bluredControl.controlConfirmation.valid = valid;
        formControls[bluredIndex] = bluredControl;
        this.setState({formControls});
    }

    submitRegisterForm = (event) => {
        event.preventDefault();
        let valid = true;
        const formControls = [...this.state.formControls];
        for (let i = 0, max = formControls.length; i < max; i++) {
            const choosenControl = {...formControls[i]};
            valid = valid && choosenControl.controlConfirmation.valid;
            if (choosenControl.controlConfirmation.touched !== true) {
                choosenControl.controlConfirmation.touched = true;
                formControls[i] = choosenControl;
                return this.setState({formControls});
            }
        }
        if (valid) {
            const data = {
                username: formControls[0].value,
                email: formControls[1].value,
                password: formControls[2].value,
                re_password: formControls[3].value
            };
            this.props.onSubmitRegisterForm(data);
        }
    }
    
    render() { 
        let register = (
            <section id="register">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6">
                            <div>
                                <h1 className="header-register">Welcome to our blog!</h1>                            </div>
                        </div>
                        <div className="col-md-6">
                            <div>
                                <h2 className="main-header-h2">Join us</h2>
                                <form onSubmit={this.submitRegisterForm}>
                                {/* this.props.registerError ? 
                                    <div className="alert alert-invalid">
                                        <div>{this.props.registerError}</div>
                                    </div> 
                                : null */}
                                    {this.state.formControls.map(control => (
                                        <Input  
                                                blur={this.blurHandler}
                                                errors={control.controlConfirmation.errors}
                                                valid={control.controlConfirmation.valid}
                                                touched={control.controlConfirmation.touched}
                                                focus={this.focusHandler}
                                                id={control.id}
                                                change={this.changeHandler}
                                                value={control.value}
                                                key={control.id}
                                                type={control.type} 
                                                placeholder={control.placeholder}
                                                iconClass={control.iconClass}/>
                                    ))}
                                    <div className="form-register-button">
                                        <Button type="submit" text={this.props.isLoading ? <Loading /> : "Sign Up"}/>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );

        if (this.props.registered) {
            register = <Redirect to="/login"/>
        }

        if (this.props.loggedIn) {
            register = <Redirect to="/m/posts"/>
        }

        return register;
    }
}

const mapStateToProps = state => {
    return {
        isLoading: state.auth.isLoading,
        registerError: state.auth.registerError,
        registered: state.auth.registered,
        loggedIn: state.auth.token ? true : false
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSubmitRegisterForm: (data) => dispatch(authActions.authRegister(data))
    };
};
 
export default connect(mapStateToProps, mapDispatchToProps)(HomeRegister);