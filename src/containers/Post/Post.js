import React, { Component } from 'react';

class Post extends Component {

    componentDidMount() {
        console.log(this.props.match.params.id);
    }

    render() {
        return (
            <p>Post page</p>
        );
    }

}
 
export default Post;