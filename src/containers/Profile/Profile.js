import React, { Component } from 'react';
import './Profile.css';
import Button from '../../components/Form/Button/Button';
import { connect } from 'react-redux';
import NavItem from '../../components/Navbar/NavItem/NavItem';
import Page404 from '../../containers/Page404/Page404';
import { Route, Switch } from 'react-router-dom';
import Setting from '../../components/Profile/Setting/Setting'; 
import Auxiliar from '../../hoc/Auxiliar/Auxiliar';
import Following from '../../components/Profile/Following/Following'; 
import Followers from '../../components/Profile/Followers/Followers'; 
import MyPosts from '../../components/Profile/MyPosts/MyPosts';
import * as authActions from '../../store/auth/authActions';

class Profile extends Component {

    followHandler = (followingId, followersId) => {
        const token = this.props.token || localStorage.getItem("token");
        this.props.onFollowUser(followingId, followersId, this.props, `Berar ${token}`);
    }

    render() {
        let navItems = (
            <Auxiliar>
                <NavItem exact to={`${this.props.match.url}`} text="posts"/>
                <NavItem to={`${this.props.match.url}/followers`} text="followers"/>
                <NavItem to={`${this.props.match.url}/following`} text="following"/>
                <NavItem to={`${this.props.match.url}/setting`} text="setting"/>
            </Auxiliar>
        );
        let follow = null;
        let routes = (
            <Switch>
                <Route exact path={`${this.props.match.url}`} render={() => <MyPosts id={this.props.match.params.id}/>} />
                <Route path={`${this.props.match.url}/setting`} render={() => <Setting id={this.props.match.params.id}/>} />
                <Route path={`${this.props.match.url}/Following`} render={() => <Following id={this.props.match.params.id} />}  />
                <Route path={`${this.props.match.url}/Followers`} render={() => <Followers id={this.props.match.params.id} />}  />
                <Route path="*" component={Page404}/>
            </Switch>
        );
        if (this.props.id !== this.props.match.params.id) {
            let followingOrNot = false;
            for (let i = 0, max = this.props.following.length; i < max; i++) {
                if (this.props.following[i].id === this.props.match.params.id) {
                    followingOrNot = true;
                    break;
                }
            }

            follow = (
                <Auxiliar>
                    <div className="spacer"></div>
                    <li>
                        <Button 
                            onClick={() => this.followHandler(this.props.match.params.id, this.props.id)} 
                            text={followingOrNot ? <i class="fas fa-check"></i> : "Follow"}/>
                    </li>
                </Auxiliar>
            );
            
            if (!this.props.email || !this.props.id || !this.props.token) {
                follow = (
                    <Auxiliar>
                        <div className="spacer"></div>
                    </Auxiliar>
                );
            }

            navItems = (
                <Auxiliar>
                    <NavItem exact to={`${this.props.match.url}`} text="posts"/>
                    <NavItem to={`${this.props.match.url}/followers`} text="followers"/>
                    <NavItem to={`${this.props.match.url}/following`} text="following"/>
                </Auxiliar>
            );
            routes = (
                <Switch>
                    <Route exact path={`${this.props.match.url}`} render={() => <MyPosts id={this.props.match.params.id}/>} />
                    <Route path={`${this.props.match.url}/Following`} render={() => <Following id={this.props.match.params.id} />}  />
                    <Route path={`${this.props.match.url}/Followers`} render={() => <Followers id={this.props.match.params.id} />}  />
                    <Route path="*" component={Page404}/>
                </Switch>
            );
        }
        
        return (
            <section id="profile">
                <div className="container">
                    <div className="row">
                        <ul className="profile-controllers">
                            {navItems}
                            {follow}
                        </ul>
                        <div style={{width: "100%"}}>
                            {routes}
                        </div>
                    </div>
                </div>
            </section>
        );
    }

}
 

const mapStateToProps = state => {
    return {
        email: state.auth.email,
        id: state.auth.id,
        token: state.auth.token,
        following: state.auth.following
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onFollowUser: (followingId, followersId, props, token) => dispatch(authActions.followUserHandler(followingId, followersId, props, token))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);