import React, { Component } from 'react';
import NavItem from '../../components/Navbar/NavItem/NavItem';
import './Navbar.css';
import Auxiliar from '../../hoc/Auxiliar/Auxiliar';
import Logo from '../../assets/logo.png';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as authActions from '../../store/auth/authActions';

class Navbar extends Component {

    render() {

        let links = (
            <Auxiliar>
                <NavItem to="/" exact text="register"/>
                <NavItem to="/Login" text="Login"/>
            </Auxiliar>
        );

        if (this.props.loggedIn) {
            links = (
                <Auxiliar>
                    <NavItem to="/m/posts" text="Home"/>
                    <NavItem to={"/m/profile/" + this.props.id} text={this.props.username}/>
                    <li onClick={() => this.props.logoutHandler(this.props)}><a style={{cursor: "pointer"}}>Logout</a></li>
                </Auxiliar>
            );
        }

        return (
            <nav className="navbar">
                <div className="container">
                    <div className="row">
                        <div className="site-logo">
                        <img src={Logo} alt="logo" style={{maxWidth: '100px'}}/>
                    </div>
                    <ul>
                        {links}
                    </ul>
                    </div>
                </div>
            </nav>
        );
    }

}

const mapStateToProps = state => {
    return {
        loggedIn: state.auth.token ? true : false,
        username: state.auth.username,
        id: state.auth.id
    };
};

const mapDispatchToProps = dispatch => {
    return {
        logoutHandler: (props) => dispatch(authActions.logout(props))
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Navbar));