import React, { Component } from 'react';
import Input from '../../components/Form/Input/Input';
import Button from '../../components/Form/Button/Button';
import './Login.css';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as authActions from '../../store/auth/authActions';
import Loading from '../../components/Loading/Loading';

class Login extends Component {


    componentDidMount() {
        console.log(this.props.match);
    }

    state = {
        formControls: [
            {
                id: "login-username-input",
                type: "email",
                placeholder: "Email*",
                iconClass: "register-input-email",
                value: "",
                controlConfirmation: {
                    touched: false,
                    valid: false,
                    // eslint-disable-next-line
                    regex: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
                }
            },
            {
                id: "login-password-input",
                type: "password",
                placeholder: "Password*",
                iconClass: "register-input-password",
                value: "",
                controlConfirmation: {
                    touched: false,
                    valid: false
                }
            }
        ]
    };

    changeHandler = (event, id) => {
        const formControls = [...this.state.formControls];
        const changedIndex = formControls.findIndex(control => control.id === id);
        const changedControl = {...formControls[changedIndex]};
        changedControl.value = event.target.value;
        formControls[changedIndex] = changedControl;
        this.setState({formControls});
    }

    blurHandler = (event, id) => {
        const formControls = [...this.state.formControls];
        const bluredIndex = formControls.findIndex(control => control.id === id);
        const bluredControl = {...formControls[bluredIndex]};
        let valid = true;

        if (bluredControl.type === "email") {
            valid = valid && bluredControl.controlConfirmation.regex.test(event.target.value);
        } else {
            valid = valid && bluredControl.value.length >= 6;
        }
        bluredControl.controlConfirmation.valid = valid;
        formControls[bluredIndex] = bluredControl;
        this.setState({formControls});
    }

    focusHandler = (id) => {
        const formControls = [...this.state.formControls];
        const focusedIndex = formControls.findIndex(control => control.id === id);
        const focusedControl = {...formControls[focusedIndex]};
        if (focusedControl.controlConfirmation.touched === false) {
            focusedControl.controlConfirmation.touched = true;
            formControls[focusedIndex] = focusedControl;
            this.setState({formControls});
        }
    }

    submitRegisterForm = (event) => {
        event.preventDefault();
        let valid = true;
        const formControls = [...this.state.formControls];
        for (let i = 0, max = formControls.length; i < max; i++) {
            const choosenControl = {...formControls[i]};
            valid = valid && choosenControl.controlConfirmation.valid;
            if (choosenControl.controlConfirmation.touched !== true) {
                choosenControl.controlConfirmation.touched = true;
                formControls[i] = choosenControl;
                return this.setState({formControls});
            }
        }
        if (valid) {
            const data = {
                email: formControls[0].value,
                password: formControls[1].value
            };
            this.props.onSubmitLoginForm(data);
        }
    }

    render() {

        let login = (
            <section style={{marginTop: "100px"}}>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-6 col-md-7 col-sm-8 col-10" style={{margin: "auto"}}>
                            <div>
                                <h2 className="main-header-h2">login</h2>
                                <form onSubmit={this.submitRegisterForm}>
                                    {this.props.loginError ? <div className="alert alert-invalid">
                                        <div>{this.props.loginError}</div>
                                    </div> : null}
                                    {this.state.formControls.map(control => (
                                        <Input  
                                                blur={this.blurHandler}
                                                focus={this.focusHandler}
                                                touched={control.controlConfirmation.touched}
                                                valid={control.controlConfirmation.valid}
                                                change={this.changeHandler}
                                                id={control.id}
                                                value={control.value}
                                                key={control.id}
                                                type={control.type} 
                                                placeholder={control.placeholder}
                                                iconClass={control.iconClass}/>
                                    ))}
                                    <div className="forget-link">
                                        <Link to="/forgotpass">Forgot Password?</Link>
                                    </div>
                                    <div className="form-login-button">
                                        <Button type="submit" text={this.props.isLoading ? <Loading /> : "sign in"}/>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );

        if (this.props.loggedIn) {
            login = <Redirect to="/m/posts"/>
        }

        return login;
    }

}
 
const mapStateToProps = state => {
    return {
        isLoading: state.auth.isLoading,
        loginError: state.auth.loginError,
        loggedIn: state.auth.token ? true : false
    };
}

const mapDispatchToProps = dispatch => {
    return {
        onSubmitLoginForm: (data) => dispatch(authActions.authLogin(data)),
        resetRegistered: () => dispatch({type: authActions.LOGIN_RESET_REGISTERED})
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);