import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as authActions from '../../store/auth/authActions';
import './posts.css';
import PostPublisher from '../../components/Posts/PostPublisher';
import Post from '../../components/Posts/Post';
import Model from '../../components/Posts/Model/Model';

class Posts extends Component {

    state = {
        post: {
            value: ""
        }
    };

    componentDidMount() {
        if (!this.props.posts.length) {
            const token = this.props.token || localStorage.getItem("token");
            this.props.checkAuthAndGetPosts(this.props, `Berar ${token}`);
        }
    }

    changeHandler = (event) => {
        const post = {...this.state.post};
        post.value = event.target.value;
        this.setState({post});
    };


    onSubmitHandler = (event) => {
        event.preventDefault();
        const Editpost = {...this.state.post};
        const text = Editpost.value;
        if (text.length > 0) {
            Editpost.value = "";
            this.setState({post: Editpost});
            const token     = this.props.token    || localStorage.getItem("token");
            const id        = this.props.id       || localStorage.getItem("id");
            const username  = this.props.username || localStorage.getItem("username");
            const post = {text, id, username};
            this.props.publishAPost(this.props, `Berar ${token}`, post);
        }
    }
    

    likeClickHandler = (postId) => {
        const id    = this.props.id       || localStorage.getItem("id");
        const token = this.props.token    || localStorage.getItem("token");
        this.props.onLikeAPost(postId, id, `Berar ${token}`);
    }
    
    onSubmitPostHandler = (e, postId, text) => {
        e.preventDefault();
        if (this.state.value !== "") {
            const props = this.props;
            const token = this.props.token || localStorage.getItem("token");
            const id = this.props.id || localStorage.getItem("id");
            const username = this.props.username || localStorage.getItem("username");
            this.props.addNewComment(props, `Berar ${token}`, id, username, text, postId);
        }
    }

    shareHandler = (text, postId) => {
        const props = this.props;
        const token = this.props.token || localStorage.getItem("token");
        const id = this.props.id || localStorage.getItem("id");
        const username = this.props.username || localStorage.getItem("username");
        const post = {text, id, username, postId};
        this.props.onShareHandler(props, `Berar ${token}`, post);
    };

    render() {

        let posts = null;
        if (this.props.posts) {
            posts = (
                this.props.posts.map((post, index) =>
                    <Post   key={post._id}
                            _id={post._id}
                            onStartShare={this.props.onStartShare}
                            onSubmitHandler={this.onSubmitPostHandler}
                            id={post.publisherId} 
                            text={post.text}
                            username={post.publisherUsernamer}
                            noOflikes={post.likes.length}
                            comments={post.comments}
                            noOfShares={post.shares.length}
                            likedByMe={post.likedByMe}
                            likeClickHandler={() => this.likeClickHandler(post._id)}/>
                )
            );
        }

        return (
            <section id="posts">
                <div className="container">
                    <div className="row">
                        <PostPublisher  onSubmitHandler={this.onSubmitHandler} 
                                        value={this.state.post.value} 
                                        changePostHandler={this.changeHandler}/>
                        {posts}
                        <Model 
                            shareHandler={this.shareHandler}
                            onCancelShare={this.props.onCancelShare}/>
                    </div>
                    {this.props.morePosts ? <div className="showMoreButton"><span onClick={this.props.showMorePosts}>Show More</span></div> : null}
                </div>
            </section>
        );
    }

}

const mapStateToProps = state => {
    return {
        token: state.auth.token,
        posts: state.auth.posts,
        id: state.auth.id,
        username: state.auth.username,
        morePosts: state.auth.morePosts
    };
};

const mapDisptachToProps = dispatch => {
    return {
        checkAuthAndGetPosts: (props, token) => dispatch(authActions.checkAuthAndGetPosts(props, token)),
        publishAPost: (props, token, post) => dispatch(authActions.publishAPost(props, token, post)),
        onLikeAPost: (postId, id, token) => dispatch(authActions.likeAPost(postId, id, token)),
        showMorePosts: () => dispatch(authActions.showMorePosts()),
        addNewComment: (props, token, id, username, text, postId) => dispatch(authActions.addNewComment(props, token, id, username, text, postId)),
        onStartShare: (text, postId) => dispatch(authActions.startShare(text, postId)),
        onCancelShare: () => dispatch(authActions.cancelShare()),
        onShareHandler: (props, token, post) => dispatch(authActions.shareHandler(props, token, post))
    };
};


export default connect(mapStateToProps, mapDisptachToProps)(Posts);