import React, { Component } from 'react';
import Input from '../../components/Form/Input/Input';
import Button from '../../components/Form/Button/Button';
import { connect } from 'react-redux';
import * as authActions from '../../store/auth/authActions';
import Loading from '../../components/Loading/Loading';

class ForgotPassword extends Component {

    state = {
        email: "",
        formControls: [
            {
                type: "email",
                placeholder: "Email*",
                iconClass: "register-input-email",
                value: "",
                controlConfirmation: {
                    // eslint-disable-next-line
                    regex: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
                }
            }
        ]
    };

    changeHandler = (event) => {
        const formControls = [...this.state.formControls];
        const changedControl = {...formControls[0]};
        changedControl.value = event.target.value;
        formControls[0] = changedControl;
        this.setState({formControls, email: event.target.value});
    }

    submitRegisterForm = (event) => {
        event.preventDefault();
        const emailControl = {...this.state.formControls[0]};
        let valid = emailControl.controlConfirmation.regex.test(emailControl.value);
        if (valid) {
            const data = {
                email: emailControl.value
            };
            this.props.onSubmitLoginForm(data);
        }
    }

    render() { 
        return (
            <section style={{marginTop: "100px"}}>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-6 col-md-7 col-sm-8 col-10" style={{margin: "auto"}}>
                            <div>
                                <h2 className="main-header-h2">Reset Password</h2>
                                <p style={{color: "#FFF"}}>Enter your email address and we’ll send you an email with instructions to reset your password.</p>
                                <form onSubmit={this.submitRegisterForm}>
                                    {this.props.changePasswordMess ? 
                                        <div className="alert alert-valid">
                                            <div>{this.props.changePasswordMess + this.state.email}</div>
                                        </div> 
                                    : null}
                                    <Input  
                                        change={this.changeHandler}
                                        value={this.state.formControls[0].value}
                                        type={this.state.formControls[0].type} 
                                        placeholder={this.state.formControls[0].placeholder}
                                        iconClass={this.state.formControls[0].iconClass}/>
                                    <div className="form-login-button">
                                        <Button type="submit" text={this.props.isLoading ? <Loading /> : "send"}/>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }

}
 
const mapStateToProps = state => {
    return {
        isLoading: state.auth.isLoading,
        changePasswordMess: state.auth.changePasswordMess
    };
}

const mapDispatchToProps = dispatch => {
    return {
        onSubmitLoginForm: (data) => dispatch(authActions.authForgotPass(data))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);