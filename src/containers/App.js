import React, { Component } from 'react';
import Layout from '../hoc/Auxiliar/Auxiliar';
// import asyncComponent from '../hoc/asyncComponent/asyncComponent';
import Register from './Register/Register';
import Login from './Login/Login';
import ForgotPassword from './ForgotPassword/ForgotPassword';
import Navbar from './Navbar/Navbar';
import Posts from './Posts/Posts';
import Post from './Post/Post';
import Profile from './Profile/Profile';
import Page404 from './Page404/Page404';
import ChangePassword from '../components/ChangePassword/ChangePassword';
import Footer from '../components/Footer/Footer';
import { connect } from 'react-redux';
import * as authActions from '../store/auth/authActions';
import { Switch, Route, withRouter } from 'react-router-dom';
import './App.css';

/**
 *
 * @description
 * initialization the whole project and creating the whole app router 
 * width react-router-dom v4.3.1
 * 
 * @class App
 * @extends {Component}
 */

// const asyncPosts = asyncComponent(() => {
//   return import('./Posts/Posts');
// });


class App extends Component {

  componentDidMount() {
    this.props.loginUser();
  }

  render() {
    return (
      <Layout>
        <main className="main-container">
          <Navbar />
          <Switch>
            <Route path="/m/posts" component={Posts}/>
            <Route path="/m/post/:id" component={Post}/>
            <Route path="/m/profile/:id" component={Profile}/>
            <Route path="/Login" component={Login}/>
            <Route path="/forgotpass" component={ForgotPassword}/>
            <Route path="/" exact component={Register}/>
            <Route path="/:number/:id" component={ChangePassword}/>
            <Route path="*" component={Page404}/>
          </Switch>
          <Footer />
        </main>
      </Layout>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    loginUser: () => dispatch(authActions.login())
  };
};

export default withRouter(connect(null, mapDispatchToProps)(App));
