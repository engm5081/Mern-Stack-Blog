import * as actions from './authActions';

const initState = {
    isLoading: false,
    registerError: null,
    loginError: null,
    forgotError: null,
    registered: false,
    email: null, 
    expiresIn: null, 
    id: null, 
    success: null, 
    token: null, 
    username: null,
    roles: null,
    followers: null,
    following: [],

    allPosts: [],
    lastShownIndex: null,
    morePosts: true,
    posts: [],
    showShare: false,
    animateShare: false,
    startShareLoading: false,
    postSharedText: null,
    postSharedId: null,

    /* messages */
    changePasswordMess: null
};

const likeAPost = (state, action) => {
  let posts = [...state.posts];
  const postIndex = posts.findIndex(post => post._id === action.post._id);
  const likeOrUnLike = action.post.likes.findIndex(like => like.id === state.id);
  posts[postIndex] = action.post;
  if (likeOrUnLike !== -1) {
    posts[postIndex].likedByMe = true;
  }
  return {
    ...state,
    posts
  };
};

const logoutUser = (state) => {
  const resetState = {...state};
  for (const key in resetState) {
    if (key === "posts" || key === "allPosts") {
      resetState[key] = [];
    } else {
      resetState[key] = null;
    }
  }
  return resetState;
};

const isLikedByMe = (id, post) => {
  let aPost = {...post};
  const likeOrUnLike = post.likes.findIndex(like => like.id === id);
  if (likeOrUnLike !== -1) {
    aPost.likedByMe = true;
  }
  return aPost;
};

const getAllPostsAndSetPosts = (state, posts) => {
  let shownPosts, lastShownIndex, morePosts;
  if (posts.length > 10) {
    shownPosts = posts.slice(0, 10);
    lastShownIndex = 10;
    morePosts = true;
  } else {
    const lengthOfPosts = posts.length;
    shownPosts = posts.slice(0, lengthOfPosts);
    lastShownIndex = lengthOfPosts;
    morePosts = false;
  }
  shownPosts = shownPosts.map(post => {
    return isLikedByMe(state.id, post);
  });
  return {
    ...state,
    allPosts: posts,
    posts: shownPosts,
    lastShownIndex,
    morePosts
  };
}

const showMorePosts = (state) => {
  const copiedState = {...state};
  if (!copiedState.morePosts) return copiedState;
  const {allPosts, posts, lastShownIndex} = copiedState;
  let newPosts, newLastShownIndex, newMorePosts;
  if (allPosts.length > lastShownIndex + 10) {
    newPosts = posts.concat(allPosts.slice(lastShownIndex, lastShownIndex + 10));
    newLastShownIndex = lastShownIndex + 10;
    newMorePosts = true;
  } else {
    newPosts = posts.concat(allPosts.slice(lastShownIndex, lastShownIndex + posts.length));
    newLastShownIndex = lastShownIndex + posts.length;
    newMorePosts = false;
  }
  newPosts = newPosts.map(post => {
    return isLikedByMe(state.id, post);
  });
  return {
    ...state,
    posts: newPosts,
    lastShownIndex: newLastShownIndex,
    morePosts: newMorePosts
  };
};

const addACommentToAPost = (state, post) => {
  const posts = [...state.posts];
  const selectedPostIndex = posts.findIndex(statePost => statePost._id === post._id);
  const selectedPost = {...posts[selectedPostIndex]};
  selectedPost.comments = post.comments;
  posts[selectedPostIndex] = selectedPost;
  return {
    ...state,
    posts
  };
}

const editSharedPost = (state, newPosts) => {
  const {post, updatedPost} = newPosts;
  const posts = [...state.posts];
  posts.push(post);
  const changedPostIndex = posts.findIndex(post => post._id === updatedPost._id);
  const selectChangedPost = posts[changedPostIndex];
  selectChangedPost.shares = updatedPost.shares;
  posts[changedPostIndex] = selectChangedPost;
  return {
    ...state,
    posts
  }
};

const authReducer = (state = initState, action) => {
    switch(action.type) {
        case(actions.REGISTER_START): return {...state, isLoading: true, loginError: null};
        case(actions.REGISTER_END): return {...state, isLoading: false, loginError: null};
        case(actions.REGISTER_ERROR): return {...state, registerError: action.value}; 
        case(actions.REGISTERED): return {...state, registered: true}; 
        case(actions.LOGIN_START): return {...state, isLoading: true, registerError: null};
        case(actions.LOGIN_END): return {...state, isLoading: false, registerError: null};
        case(actions.LOGIN_ERROR): return {...state, loginError: action.value};
        case(actions.LOGIN_RESET_REGISTERED): return {...state, registered: false};
        case(actions.AUTH_LOGIN): return {...state, ...action.data};
        case(actions.LOGIN_USER): return {...state, ...action.data};
        case(actions.LOGOUT_USER): return logoutUser(state);
        case(actions.FORGOTPASSWORD_START): return {...state, isLoading: true, loginError: null, registerError: null};
        case(actions.FORGOTPASSWORD_END): return {...state, isLoading: false, loginError: null, registerError: null};
                
        case(actions.CHECK_AUTH_AND_GET_POSTS): return getAllPostsAndSetPosts(state, action.posts);
        case(actions.LIKE_A_POST): return likeAPost(state, action);
        case(actions.NEW_POST_PUBLISH): return {...state, posts: [action.newPost, ...state.posts]};
        case(actions.SHOW_MORE_POSTS): return showMorePosts(state);
        case(actions.ADD_NEW_COMMENT): return addACommentToAPost(state, action.post);
        case(actions.START_SHARE): return {...state, showShare: true, postSharedText: action.payload.text, postSharedId: action.payload.postId};
        case(actions.START_SHARE_ANIMATION): return {...state, animateShare: true};
        case(actions.START_CANCEL_SHARE): return {...state, animateShare: false, postSharedText: null, postSharedId: null};
        case(actions.END_CANCEL_SHARE): return {...state, showShare: false};
        case(actions.SHARE_STARTING): return {...state, startShareLoading: true};
        case(actions.EDIT_SHARED_POST): return editSharedPost(state, action.posts)
        case(actions.SHARE_ENED): return {...state, startShareLoading: false, showShare: false, animateShare: false};
        case(actions.FOLLOW_USER): return {...state, following: action.user.following}
        
        case(actions.SUBMIT_NODE_FORM): return {...state, changePasswordMess: action.mess}
        default: return state;
    }
};

export default authReducer;