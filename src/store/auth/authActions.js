import axios from 'axios';

export const REGISTER_START = "REGISTER_START";
export const REGISTERED = "REGISTERED";
export const REGISTER_END = "REGISTER_END";
export const REGISTER_ERROR = "REGISTER_ERROR";
export const authRegister = data => {
    return dispatch => {
        dispatch({type: REGISTER_START});
        axios.post("/user/register", data)
            .then(response => {
                console.log(response);
                dispatch({type: REGISTERED});
                dispatch({type: REGISTER_END});
                dispatch({type: REGISTER_ERROR, value: null});
            })
            .catch(error => {
                console.log(error);
                dispatch({type: REGISTER_END});
                dispatch({type: REGISTER_ERROR, value: "Something Went Wrong"});
            });
    };
};


export const LOGIN_START = "LOGIN_START";
export const AUTH_LOGIN = "AUTH_LOGIN";
export const LOGIN_RESET_REGISTERED = "LOGIN_RESET_REGISTERED";
export const LOGIN_END = "LOGIN_END";
export const LOGIN_ERROR = "LOGIN_ERROR";
export const authLogin = data => {
    return dispatch => {
        dispatch({type: LOGIN_START});
        axios.post("/user/login", data)
            .then(response => {
                const data = {...response.data};
                const {success} = data;
                console.log(data);
                delete data.success;
                delete data.success;
                if (success) {
                    for (let key in data) {
                        if (key === "roles" || key === "following") {
                            localStorage.setItem(key, JSON.stringify(data[key]));
                        } else {
                            localStorage.setItem(key, data[key]);
                        }
                    }
                    dispatch({type: AUTH_LOGIN, data});
                    dispatch({type: LOGIN_ERROR, value: null});
                } else {
                    dispatch({type: LOGIN_ERROR, value: "Something Went Wrong"});
                }
                dispatch({type: LOGIN_END});
            })
            .catch(error => {
                console.log(error);
                dispatch({type: LOGIN_END});
                dispatch({type: LOGIN_ERROR, value: "Something Went Wrong"});
            });
    };
};

export const FORGOTPASSWORD_START = "FORGOTPASSWORD_START";
export const FORGOTPASSWORD_END = "FORGOTPASSWORD_END";
export const SUBMIT_NODE_FORM = "SUBMIT_NODE_FORM";
export const authForgotPass = data => {
    return dispatch => {
        dispatch({type: FORGOTPASSWORD_START});
        axios.post("/user/forgotpass", data)
            .then(response => {
                if (!response.data) console.log("wrong");
                dispatch({type: LOGIN_END});
                dispatch({type: SUBMIT_NODE_FORM, mess: "Email Was Sent To "});
            })
            .catch(error => {
                console.log(error);
                dispatch({type: FORGOTPASSWORD_END});
            });
    };
};

export const LOGOUT_USER = "LOGOUT_USER";
export const logout = (props) => {
    return dispatch => {
        props.history.push("/login");
        const logoutItems = {"email": null, "expiresIn": null, "id": null, "success": null, "token": null, "username": null, "roles": null, "following": []};
        for (let key in logoutItems) {
            localStorage.removeItem(key);
        }
        dispatch({type: LOGOUT_USER});
    };
};


export const LOGIN_USER = "LOGIN_USER";
export const login = () => {
    return dispatch => {
        const loginItems = {"email": null, "expiresIn": null, "id": null, "success": null, "token": null, "username": null, "roles": null, "following": null};
        for (let key in loginItems) {
            if (key === "following" || key === "roles") {
                loginItems[key] = JSON.parse(localStorage.getItem(key));
            } else {
                loginItems[key] = localStorage.getItem(key);
            }
        }
        let time = loginItems.expiresIn ? +loginItems.expiresIn : 0 - new Date().getTime();
        time = time > 0 ? time : 0;
        setTimeout(() => {
            // refresh token
            console.log("new token is needed");
        }, time);
        dispatch({type: LOGIN_USER, data: loginItems});
    };
};


/* posts page */
export const CHECK_AUTH_AND_GET_POSTS = "CHECK_AUTH_AND_GET_POSTS";
export const checkAuthAndGetPosts = (props, token) => {
    return dispatch => {
        axios.get("/main/posts", {
            headers: {authorization: token}
        })
            .then(response => {
                if (!response.data) {
                    props.history.push("/");
                    return dispatch(logout(props));
                }
                const posts = response.data.sort((a,b) => new Date(b.date).getTime() - new Date(a.date).getTime());
                dispatch({type: CHECK_AUTH_AND_GET_POSTS, posts});
            })
            .catch(err => {
                props.history.push("/");
                dispatch(logout(props));
            });
    };
};

export const NEW_POST_PUBLISH = "NEW_POST_PUBLISH";
export  const publishAPost = (props, token, post) => {
    console.log("test3");
    return dispatch => {
        if (post.text.length) {
            axios.post("/post/add", post, {
                headers: {authorization: token}
            })
                .then(response => {
                    if (!response.data) {
                        props.history.push("/");
                        return  dispatch(logout(props));
                    }
                    dispatch({type: NEW_POST_PUBLISH, newPost: response.data});
                })
                .catch(err => {
                    props.history.push("/");
                    dispatch(logout(props));
                });
        }
    };
};


/* like post */
export const LIKE_A_POST = "LIKE_A_POST";
export const likeAPost = (postId, id, token) => {
    return dispatch => {
        axios.post("/post/like", {postId, id}, {
            headers: {authorization: token}
        })
            .then(response => {
                if (response.data) {
                    dispatch({type: LIKE_A_POST, post: response.data})
                }
            })
            .catch(_ => {});
    };
};


export const SHOW_MORE_POSTS = "SHOW_MORE_POSTS";
export const showMorePosts = () => {
    return dispatch => {
        dispatch({type: SHOW_MORE_POSTS})
    };
};

export const FOLLOW_USER = "FOLLOW_USER";
export const FOLLOW_USER_HANDLER = "FOLLOW_USER_HANDLER";
export const followUserHandler = (followingId, followersId, props, token) => {
    return dispatch => {
        axios.post("/user/follow", {followingId, followersId}, {
            headers: {authorization: token}
        })
            .then(response => {
                if (!response.data) return props.history.push("/login");
                localStorage.setItem("following", JSON.stringify(response.data.following));
                dispatch({type: FOLLOW_USER, user: response.data});
            })
            .catch(_ => {
                props.history.push("/login");
            });

    };
};

export const ADD_NEW_COMMENT = "ADD_NEW_COMMENT";
export const addNewComment = (props, token, id, username, text, postId) => {
    return dispatch => {
        axios.post("/post/addcomment", {id, username, text, postId}, {
            headers: {authorization: token}
        })
            .then(response => {
                if (!response.data) return props.history.push("/login");
                dispatch({type: ADD_NEW_COMMENT, post: response.data})
            })
            .catch(_ => {
                props.history.push("/login");
            });
    };
};

export const START_SHARE = "START_SHARE";
export const START_SHARE_ANIMATION = "START_SHARE_ANIMATION";
export const startShare = (text, postId) => {
    return dispatch => {
        dispatch({type: START_SHARE, payload: {text, postId}});
        setTimeout(() => {
            dispatch({type: START_SHARE_ANIMATION});
        }, 1);
    };
};

export const START_CANCEL_SHARE = "START_CANCEL_SHARE";
export const END_CANCEL_SHARE = "END_CANCEL_SHARE";
export const cancelShare = () => {
    return dispatch => {
        dispatch({type: START_CANCEL_SHARE});
        setTimeout(() => {
            dispatch({type: END_CANCEL_SHARE});
        }, 350);
    };
};

export const SHARE_STARTING = "SHARE_STARTING";
export const SHARE_ENED = "SHARE_ENED";
export const EDIT_SHARED_POST = "EDIT_SHARED_POST";
export const shareHandler = (props, token, post) => {
    return dispatch => {
        if (post.text.length) {
            dispatch({type: SHARE_STARTING});
            axios.post("/post/share", post, {
                headers: {authorization: token}
            })
                .then(response => {
                    if (!response.data) {
                        props.history.push("/");
                        return  dispatch(logout(props));
                    }
                    dispatch({type: EDIT_SHARED_POST, posts: response.data});
                    dispatch({type: START_CANCEL_SHARE});
                    setTimeout(() => {
                        dispatch({type: END_CANCEL_SHARE});
                        dispatch({type: SHARE_ENED});
                    }, 350);
                })
                .catch(_ => {
                    props.history.push("/");
                    dispatch(logout(props));
                });
        }
    };
};

export const submitNodeForm = (data, props) => {
    return _ => {
        axios.post(`/user/forget/${data.id}`, data)
            .then(response => {
                if (!response.data) return props.history.push("/");
                props.history.push("/login");
            })
            .catch(_ => {
                props.history.push("/");
            })
    };
};