import React from 'react';
import './Input.css';

const input = (props) => {
    return (
        <section>
            <div className={
                            "form-input " + 
                            props.iconClass +
                            (props.touched ? (props.valid ? " input-valid" : " input-invalid") : "")}>
                <input 
                        onBlur={props.blur ? (event) => props.blur(event, props.id) : null}
                        value={props.value}
                        onChange={(event) => props.change(event, props.id)} 
                        type={props.type} 
                        placeholder={props.placeholder}
                        onFocus={props.focus ? () => props.focus(props.id) : null}/>
                <div />
            </div>
            {props.errors ? <div className="alert alert-invalid">
                    {props.errors.map(error => (
                        <div key={error}>{error}</div>
                    ))}
            </div> : null}
        </section>
    );
}
 
export default input;