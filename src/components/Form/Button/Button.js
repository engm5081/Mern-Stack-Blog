import React from 'react';
import './Button.css';

const button = (props) => {
    return (
        <button onClick={props.onClick ? props.onClick : null} className={"form-button " + (props.className ? props.className : "")} type={props.type ? props.type : "button"}>{props.text}</button>
    );
}
 
export default button;