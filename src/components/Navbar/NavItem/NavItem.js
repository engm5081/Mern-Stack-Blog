import React from 'react';
import { NavLink } from 'react-router-dom';

const navItem = (props) => {
    return (
        <li><NavLink to={props.to} exact={props.exact}>{props.text}</NavLink></li>
    );
}
 
export default navItem;