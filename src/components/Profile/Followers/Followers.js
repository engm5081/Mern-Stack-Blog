import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import Auxiliar from '../../../hoc/Auxiliar/Auxiliar';
import Loading from '../../Loading/Loading';

class Follow extends Component {

    state = {
        user: null
    }

    componentDidMount() {
        if (!this.state.user) {
            axios.get(`/user/followers`, {
                headers: {id: this.props.id}
            })
                .then(response => {
                    if (!response.data) return this.props.history.push("/login");
                    this.setState({user: response.data});
                })
                .catch(_ => {
                    this.props.history.push("/login");
                });
        }
    }
    render() {
        let followers = <div className="prof-loading"><Loading /></div>;
        if (this.state.user) {
            followers = <p className="insted-msg">{this.state.user.username} has no followers</p>
            if (this.state.user.followers.length > 0) {
                followers = this.state.user.followers.map(follower => (
                    <div className="col-12 col-md-4 col-xl-3 col-sm-6 follow-person" key={follower.id}>
                        <div>
                            <Link to={`/m/profile/${follower.id}`}>{follower.username}</Link>
                        </div>
                    </div>
                ));
            }
        }

        return (
        <Auxiliar>
            {this.state.user ?
                <section id="followers">
                    <div className="container">
                        <div className="row">
                        <h3 className="following-header">{this.state.user.username}'s Followers.</h3>
                            {followers}
                        </div>
                    </div>
                </section> 
        : null}
        </Auxiliar>
        )
    }
}
 
export default Follow;