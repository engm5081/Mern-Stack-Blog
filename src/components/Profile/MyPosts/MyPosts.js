import React, { Component } from 'react';
import Auxiliar from '../../../hoc/Auxiliar/Auxiliar';
import './MyPosts.css';
import Loading from '../../Loading/Loading';
import axios from 'axios';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

class MyPosts extends Component {

    state = {
        user: null
    }

    componentDidMount() {
        if (!this.state.user) {
            const token = this.props.token || localStorage.getItem("token");
            axios.post("/user/myposts", {publisherId: this.props.id}, {
                headers: {authorization: `Berar ${token}`}
            })
                .then(response => {
                    if (!response.data) return this.props.history.push("/login");
                    this.setState({user: response.data});
                })
                .catch(_ => {
                    this.props.history.push("/login");
                });
        }
    }

    render() {
        let posts = <div className="prof-loading"><Loading /></div>,
            welcome = null;
        if (this.state.user) {
            welcome = <h3 className="posts-header">{this.state.user.username}'s posts.</h3>
            posts = <h3 className="posts-header">{this.state.user.username} has no posts</h3>;
            if (this.state.user.posts.length > 0) {
                posts = this.state.user.posts.map(post => (  
                <div className="col-12 my-post" key={post._id} style={{margin: "15px 0"}}>
                    <div dangerouslySetInnerHTML={{__html: post.text}}></div>
                </div>
                ));
            }
        }
        return (
            <Auxiliar>
                {welcome}
                {posts}
            </Auxiliar>
        );
    }
}
 
const mapStateToProps = state => {
    return {
        token: state.auth.token
    };
};

export default withRouter(connect(mapStateToProps)(MyPosts));