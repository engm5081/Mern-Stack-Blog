import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import Auxiliar from '../../../hoc/Auxiliar/Auxiliar';
import './Following.css';
import Loading from '../../Loading/Loading';

class Following extends Component {

    state = {
        user: null
    }

    componentDidMount() {
        if (!this.state.user) {
            axios.get(`/user/following`, {
                headers: {id: this.props.id}
            })
                .then(response => {
                    if (!response.data) return this.props.history.push("/login");
                    this.setState({user: response.data});
                })
                .catch(_ => {
                    this.props.history.push("/login");
                });
        }
    }
    render() {
        let following = <div className="prof-loading"><Loading /></div>;

        if (this.state.user) {
            following = <p className="insted-msg">{this.state.user.username} not following anyone.</p>;
            if (this.state.user.following.length > 0) {
                following = this.state.user.following.map(following => (
                    <div className="col-12 col-md-4 col-xl-3 col-sm-6 follow-person" key={following.id}>
                        <div>
                            <Link to={`/m/profile/${following.id}`}>{following.username}</Link>
                        </div>
                    </div>
                ));
            }
        }

        return (
        <Auxiliar>
            {this.state.user ?
                <section id="following">
                    <div className="container">
                        <div className="row">
                        <h3 className="following-header">{this.state.user.username}'s Following.</h3>
                            {following}
                        </div>
                    </div>
                </section> 
        : null}
        </Auxiliar>
        )
    }
}
 
export default Following;