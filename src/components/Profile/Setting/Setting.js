import React, { Component } from 'react';
import Button from '../../../components/Form/Button/Button';
import { connect } from 'react-redux';
import * as authActions from '../../../store/auth/authActions';

class Setting extends Component {


    
    onClickHandler = (e) => {
        e.preventDefault();
        const email = this.props.email || localStorage.getItem("email");
        this.props.onSubmitLoginForm({email});
    };

    render() {
        return (
            <div>
                <br />
                {this.props.changePasswordMess ? 
                    <div className="alert alert-valid">
                        <div>{this.props.changePasswordMess + this.props.email}</div>
                    </div> 
                : null}
                <br />
                <Button onClick={this.onClickHandler} text="Request Change Password"/>
            </div>
        );
    }
}
 
const mapStateToProps = state => {
    return {
        email: state.auth.email,
        changePasswordMess: state.auth.changePasswordMess
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSubmitLoginForm: (data) => dispatch(authActions.authForgotPass(data))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Setting);