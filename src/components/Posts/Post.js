import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Post.css';


class Post extends Component {

    componentDidMount() {
        this.setState({
            text: this.props.text,
            _id: this.props._id
        })
    }

    state = {
        value: "",
        showComments: false,
        text: null,
        _id: null
    }

    changePostHandler = (event) => {
        this.setState({value: event.target.value});
    }

    onSubmit = (e) => {
        if (this.state.value.trim() !== "") {
            this.props.onSubmitHandler(e, this.props._id, this.state.value)
            this.setState({value: ""});
        }
    }

    onShowComments = () => {
        if (!this.state.showComments) {
            this.setState({showComments: true});
        }
    }

    changeStartHandler = () => {
        this.props.onStartShare(this.props.text, this.props._id);
    }

    render() {
        let comments = null;

        if (this.props.comments.length > 0) {
            comments = this.props.comments.map((comment, index) => {
                return (
                    <div key={index}>
                        <strong>
                            <Link to={"/m/profile/" + comment.id}>{comment.username}:</Link>
                        </strong>
                        {comment.text}.
                    </div>
                );
            });
        }

       

        
        
        return (
            <div className="col-12">
                <div className="published-post">
                    <div className="publisherName">
                        Published By <Link to={`/m/profile/${this.props.id}`}><strong>{this.props.username}</strong></Link>.
                    </div>
                    <p dangerouslySetInnerHTML={{ __html: this.props.text}}></p>
                    <div className="actions-buttons">
                        <span>{this.props.noOflikes} Likes</span>
                        <span>{this.props.comments.length} Comments</span>
                        <span>{this.props.noOfShares} Share</span>
                    </div>
                    <div className="actions-buttons">
                        <button onClick={this.props.likeClickHandler} className={this.props.likedByMe ? "i-liked": null}><i className="far fa-heart"></i>Like</button>
                        <button onClick={this.onShowComments} className={this.state.showComments ? "i-liked": null}><i className="far fa-comments"></i>Comment</button>
                        <button onClick={this.changeStartHandler}><i className="far fa-share-square"></i>Share</button>
                    </div>
                    {this.state.showComments ? <section className="post-comments">
                        <div>
                            <form onSubmit={this.onSubmit}>
                                <input type="text" value={this.state.value} onChange={this.changePostHandler}/>
                            </form>
                        </div>
                        <div>
                            {comments}
                        </div>
                    </section> : null}
                </div>
            </div>
        );
    }
}
 
export default Post;