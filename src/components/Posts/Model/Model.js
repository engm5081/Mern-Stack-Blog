import React, { Component } from 'react';
import Button from '../../Form/Button/Button';
import { connect } from 'react-redux';
import Loading from '../../Loading/Loading';

class Model extends Component {
    render() { 
        return (
            <section className={this.props.animateShare ? "share-model open" : "share-model"}>
                <div className="back-bone" onClick={this.props.onCancelShare}></div>
                {!this.props.startShareLoading ? <div className="model">
                    <p>Would you like to share this post?</p>
                    <div className="button">
                        <Button text="Canel" onClick={this.props.onCancelShare}/>
                        <Button text="Share" onClick={() => this.props.shareHandler(this.props.postSharedText, this.props.postSharedId)}/>
                    </div>
                </div> : <Loading className="sharing"/>}
            </section>
        );
    }
}

const mapStateToProps = state => {
    return {
        showShare: state.auth.showShare,
        animateShare: state.auth.animateShare,
        startShareLoading: state.auth.startShareLoading,
        postSharedText: state.auth.postSharedText,
        postSharedId: state.auth.postSharedId
    }
}
 
export default connect(mapStateToProps)(Model);