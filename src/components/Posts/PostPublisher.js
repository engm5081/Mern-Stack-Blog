import React from 'react';
import Button from '../Form/Button/Button';
import ContentEditable from 'react-contenteditable';

const postPublisher = (props) => {
    return (
        <div className="col-12">
            <div className="post">
                <form onSubmit={props.onSubmitHandler}>
                    <ContentEditable 
                        html={props.value}
                        onChange={props.changePostHandler}
                    />
                    <div><Button type="submit" text="Publish"/></div>
                </form>
            </div>
        </div>
    );
}
 
export default postPublisher;