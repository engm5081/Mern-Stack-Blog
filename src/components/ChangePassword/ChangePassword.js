import React, { Component } from 'react';
import Input from '../Form/Input/Input';
import Button from '../Form/Button/Button';
import axios from 'axios';
import { connect } from 'react-redux';
import * as authActions from '../../store/auth/authActions';

class ChangePassword extends Component {

    componentDidMount() {
        const {number, id} = this.props.match.params;
        axios.get(`/user/forget/${number}/${id}`)
            .then(response => {
                if (!response.data) return this.props.history.push("/");
            })
            .catch(_ => {
                this.props.history.push("/");
            })
    }

    state = {
        formControls: [
            {
                id: "password1",
                value: "",
                type: "password",
                placeholder: "New Password*",
                iconClass: "register-input-password"
            },
            {
                id: "password2",
                value: "",
                type: "password",
                placeholder: "New Password Again*",
                iconClass: "register-input-password"
            }
        ]
    };

    changeHandler = (event, id) => {
        const formControls = [...this.state.formControls];
        const changedIndex = formControls.findIndex(control => control.id === id);
        const changedControl = {...formControls[changedIndex]};
        changedControl.value = event.target.value;
        formControls[changedIndex] = changedControl;
        this.setState({formControls});
    }

    onSubmitHandler = (e) => {
        e.preventDefault();
        const value1 = this.state.formControls[0].value;
        const value2 = this.state.formControls[1].value;
        if (value1 === value2) {
            const {number, id} = this.props.match.params;
            this.props.onSubmitNodeForm({pass: value1, re_pass: value2, number, id}, this.props);
        }
    }

    render() {
        return (
            <section id="change-password">
                <div className="container">
                    <div className="row">
                        <h2 style={{textAlign: "center", color: "#FFF", fontWeight: 800, width: "100%",margin: "15px 0", textTransform: "uppercase"}}>Change Password</h2>
                        <div className="col-lg-6 col-md-7 col-sm-8 col-10" style={{margin: "auto"}}>
                            <div>
                                <form onSubmit={this.onSubmitHandler}>
                                    {this.state.formControls.map(control => (
                                        <Input change={this.changeHandler}
                                                id={control.id}
                                                value={control.value}
                                                key={control.id}
                                                type={control.type} 
                                                placeholder={control.placeholder}
                                                iconClass={control.iconClass}/>
                                        ))}
                                    <div style={{textAlign: "center"}}><Button type="submit" text="change"/></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSubmitNodeForm: (data, props) => dispatch(authActions.submitNodeForm(data, props))
    }
};

export default connect(null, mapDispatchToProps)(ChangePassword);