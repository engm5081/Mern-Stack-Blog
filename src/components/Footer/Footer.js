import React from 'react';
import './Footer.css';

const footer = () => {
    return (
        <footer className="site-footer">
            <p>All Rights Reserved © 2018 <a className="footer-anchor" rel="noopener noreferrer" href="https://www.facebook.com/mohamed.rabie.5439087" target="_blank">MRR</a></p>
        </footer>
    );
}
 
export default footer;