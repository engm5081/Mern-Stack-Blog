import React, { Component } from 'react';

/** 
 * @description
 * This higher order component accept one param which is called in special way
 * it uses special import with the following syntx
 * @argument import()
*/

const asyncComponent = (importComponent) => {
    return class extends Component {
        state = {
            component: null
        }

        componentDidMount () {
            importComponent()
                .then(cmp => {
                    this.setState({component: cmp.default});
                });
        }
        
        render () {
            const C = this.state.component;

            return C ? <C {...this.props} /> : null;
        }
    }
}

export default asyncComponent;