const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const mongoose = require("mongoose");
const config = require("./server/config/config");
const app = express();

/* connect to mongoose */
mongoose.connect(config.mongooseUrl, {useNewUrlParser: true}, () => console.log("db connected!"));
require("./server/models/user.model");
require("./server/models/post.model");

/* main middlewares */
app.use(express.static(path.join(__dirname, "build")));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

/* auth router */
const authRoutes = require("./server/routes/user.auth");
app.use("/user", authRoutes);

/* posts and profile routes */
const postsAndProfile = require("./server/routes/posts.routes");
app.use("/", postsAndProfile);

/* main router */
app.get("*", (_, res) => {
    res.sendFile(path.join(__dirname, "/build/index.html"));
});

const port = process.env.PORT || 4000;
app.listen(port, () => console.log(`server Started on port ${port}`))